﻿import React from 'react';
import { PlayerType } from "../../domain/Models";

interface IProps {
    playerType: PlayerType;
    score: string;
}

export const Player = (props: IProps) => {
    return (
        <div className={`player player-${props.playerType}`}>
            <div className="score">{props.score}</div>
            <div className="name">{`player ${props.playerType}`}</div>
        </div>
    );
}