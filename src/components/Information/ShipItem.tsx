﻿import React from 'react';

interface IProps {
    isDetected: boolean;
}

export const ShipItem =(props: IProps) => {
    return (
        <div className={`ship-item cross ${props.isDetected ? "detected cross-red" : "undetected"}`}>
        </div>
    );
}