﻿import React from 'react';
import { Ship } from "./Ship";
import { InfoShip, PlayerType } from "../../domain/Models";
import { Player } from "./Player";
import "./../../styles/information.scss";

interface IProps {
    ships: InfoShip[];
}

export const Information =(props: IProps) => {
    return (
        <div className="information">
            <div className="players">
                <Player playerType={PlayerType.First} score="00"></Player>
                <Player playerType={PlayerType.Second} score="00"></Player>
            </div>
            <div className="ships">
                {props.ships.map((ship, i) => {
                    return (<Ship key={`ship-${i}`} name={ship.name} position={ship.position}></Ship>)
                })}
            </div>
        </div>
    );
}