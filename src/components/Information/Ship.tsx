﻿import React from 'react';
import { ShipItem } from "./ShipItem";

interface IProps {
    name: string;
    position: Map<string, boolean>
}

export const Ship =(props: IProps) => {
    return (
        <div className="ship">
            <div className={`shape ${props.name}`}></div>
            <div className="status">
                {Array.from(props.position.keys())
                    .map(key => {
                    return (<ShipItem 
                        key={`position-${key}`}
                        isDetected={props.position.get(key) || false}></ShipItem>)
                })}
            </div>
        </div>
    );
}