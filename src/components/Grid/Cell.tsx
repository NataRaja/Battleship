﻿import React from 'react';
import { useState } from 'react';
import { ICell } from "../../domain/Models";

interface IProps {
    cell: ICell;
    onClick: (positionX: number, positionY: number) => void;
}

export const Cell =(props: IProps) => {
    const [state, setState] = useState({
        cell: props.cell,
        isActive: false
    });
    
    const onClickHandler = () => {
        if (!state.isActive) {
            setState({...state, isActive: true});
            
            if (state.cell.isShip) {
                props.onClick(state.cell.positionX, state.cell.positionY);
            }
        }
    };
    
    const getStatus = () => {
        if (!state.isActive) return "cell-empty";
        
        return state.cell.isShip ? "cross-red" : "cross-gray";
    }
    
    return (
        <div className={`cell cross ${getStatus()}`} onClick={onClickHandler}></div>
    );
}