﻿import React from 'react';
import { ICell } from "../../domain/Models";
import { Cell } from "./Cell";

interface IProps {
    cells: ICell[];
    onClick: (positionX: number, positionY: number) => void;
}

export const Row =(props: IProps) => {
    return (
        <div className="row">
            {props.cells.map((cell, i) => {
                return (<Cell key={`cell-${i}`} cell={cell} onClick={props.onClick}></Cell>)
            })}
        </div>
    );
}