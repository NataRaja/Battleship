﻿import React from 'react';
import { IRow, PlayerType } from "../../domain/Models";
import { Row } from "./Row";
import "./../../styles/grid.scss";

interface IProps {
    playerType: PlayerType;
    rows: IRow[];
    onClick: (positionX: number, positionY: number) => void;
}

export const Grid =(props: IProps) => {
    return (
        <div className={`grid grid-${props.playerType}-player`}>
            {props.rows.map((row, i) => {
                return (<Row key={`row-${i}`} cells={row.cells} onClick={props.onClick}></Row>)
            })}
        </div>
    );
}