﻿import React, { useState } from 'react';
import { DataService } from "../../services/dataService";
import { GridService } from "../../services/gridService";
import { detectShip, generateInfo } from "../../services/informationService";
import { Grid } from "../Grid/Grid";
import { Information } from "../Information/Information";
import "./../../styles/playground.scss";
import { PlayerType } from "../../domain/Models";

export const Playground = () => {
    const dataService = new DataService();
    const data = dataService.get();

    const gridService = new GridService(data);

    const [state, setState] = useState(generateInfo(data));

    const onClickHandler = (positionX: number, positionY: number) => {
        const newState = detectShip(state, positionX, positionY);
        setState(newState);
    }

    return (
        <div className="container">
            <Grid onClick={onClickHandler} rows={gridService.generateGrid()} playerType={PlayerType.First}/>
            <Information ships={state}></Information>
        </div>
    );
}
