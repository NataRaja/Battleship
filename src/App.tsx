import React from 'react';
import { Playground } from "./components/Playground/Playground";

function App() {
  return (
      <Playground></Playground>
  );
}

export default App;
