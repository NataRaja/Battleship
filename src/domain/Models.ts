﻿export enum PlayerType {
    First = 1,
    Second = 2
}

export interface IRow {
    cells: ICell[];
}

export interface ICell {
    positionX: number;
    positionY: number;
    isShip: boolean;
}

export interface InfoShip {
    name: string;
    position: Map<string, boolean>
}