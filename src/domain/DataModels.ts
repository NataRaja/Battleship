﻿export interface IShip {
    name: string,
    positions: IShipPosition[]
} 

export interface IShipPosition {
    positionX: number;
    positionY: number;
}