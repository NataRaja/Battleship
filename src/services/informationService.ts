﻿import { IShip } from "../domain/DataModels";
import { InfoShip } from "../domain/Models";

export const generateInfo = (ships: IShip[]): InfoShip[] => {
    return ships.map(ship => {
        return {
            name: ship.name,
            position: new Map<string, boolean>(
                ship.positions.map(item => [getKey(item.positionX, item.positionY), false]))
        }
    });
}

export const detectShip = (data: InfoShip[], positionX: number, positionY: number):InfoShip[] => {
    const key = getKey(positionX, positionY);
    
    const result:InfoShip[] = [];

    data.forEach(infoShip => {
        if (infoShip.position.has(key)) infoShip.position.set(key, true);

        result.push(infoShip);
    });
    
    return result;
}

const getKey = (positionX: number, positionY: number): string => {
    return `position${positionX}${positionY}`;
}