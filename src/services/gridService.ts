﻿import { IShip, IShipPosition } from "../domain/DataModels";
import { ICell, IRow } from "../domain/Models";

export class GridService {
    readonly size: number = 10;
    private readonly shipPositions: IShipPosition[] = [];
    
    constructor(ships: IShip[]) {
        ships.map((item) => item.positions).forEach(positions => this.shipPositions.push(...positions));
    }

    generateGrid(): IRow[] {
        const result: IRow[] = [];
        for (let i = 0; i < this.size; i++) {
            result.push({
                cells: this.generateCells(i)
            })
        }

        return result;
    }

    private generateCells(rowIndex: number) {
        const cells: ICell[] = [];
        for (let j = 0; j < this.size; j++) {
            cells.push({
                positionX: rowIndex,
                positionY: j,
                isShip: this.isShip(rowIndex, j)
            });
        }

        return cells;
    }

    private isShip(positionX: number, positionY: number): boolean {
        return !!this.shipPositions.filter(item => item.positionX == positionX && item.positionY == positionY).length;
    }
}