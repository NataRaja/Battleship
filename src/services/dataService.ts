﻿import data from "../data/data.json";
import { IShip } from "../domain/DataModels";

export class DataService {
    get(): IShip[] {
        return data.layout.map((item) => {
            return {
                name: item.ship,
                positions: item.positions.map((positionItem) => {
                    return {
                        positionX: positionItem[0],
                        positionY: positionItem[1],
                    }
                })
            };
        });
    }
}